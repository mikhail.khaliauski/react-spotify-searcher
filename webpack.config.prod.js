"use strict";
const path = require("path");

const merge = require("webpack-merge");

const base = require("./webpack.config");

const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');



module.exports = merge(base, {
    plugins: [
        new HtmlWebpackPlugin({
            filename: path.resolve(__dirname, "build", "index.html"),
            template: path.resolve(__dirname, "public", "index.html")
        }),
        new ExtractTextPlugin({
            filename: "styles.css",
            allChunks: true
        }),
        new UglifyJSPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.scss/,
                use: ExtractTextPlugin.extract({
                    use: [
                        'css-loader',
                        'sass-loader',
                    ],
                    fallback: 'style-loader'
                })
            }
        ]
    },
});