import {LOGOUT} from "../actions/authenticationActions";
import {BEGIN_FETCH_USER, SUCCESS_FETCH_USER} from "actions/authenticationActions";

const initialState = {
    user: null,
    isPremium: false
};

export default (state = initialState, action) => {

    const {type, payload} = action;

    switch (type) {
        case BEGIN_FETCH_USER:
            return state;
        case SUCCESS_FETCH_USER:
            return {
                ...state,
                user: payload,
                isPremium: payload.product === "premium"
            };
        case LOGOUT:
            return initialState;
        default:
            return state
    }
}