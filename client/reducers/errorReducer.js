import {REMOVE_ERROR, ADD_ERROR} from "../actions/errorActions";

const initialState = [];

export default (state = initialState, action) => {
    const {type, payload} = action;

    switch (type) {
        case ADD_ERROR:
            return [...state, payload];
        case REMOVE_ERROR:
            return state.filter((item) => item.id !== payload);
        default:
            return state;
    }
}