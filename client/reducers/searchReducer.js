import {
    BEGIN_SEARCH,
    SUCCESS_SEARCH,
    UPDATE_SEARCH_TEXT
} from "../actions/searchActions";
import {DISPLAY_MODE_GRID, SEARCH_RESULTS_PER_PAGE} from "utils/constants";
import {CLEAR_SEARCH, SET_SEARCH_DISPLAY_MODE, SET_SEARCH_PAGE} from "actions/searchActions";


const initialState = {
    isLoading: false,
    searchText: "",
    searchItems: [],
    searchItemsCount: 0,
    searchItemsPage: 1,
    searchPageCount: 1,
    displayMode: DISPLAY_MODE_GRID
};


export default (state = initialState, action) => {

    const {type, payload} = action;
    switch (type) {
        case BEGIN_SEARCH:
            return {
                ...state,
                isLoading: true,
            };
        case SUCCESS_SEARCH:
            return {
                ...state,
                isLoading: false,
                searchItems: payload.items,
                searchItemsPage: payload.page,
                searchItemsCount: payload.total,
                searchPageCount: Math.ceil(payload.total / SEARCH_RESULTS_PER_PAGE)
            };
        case CLEAR_SEARCH:
            return {
                ...state,
                ...initialState,
                displayMode: state.displayMode
            };
        case UPDATE_SEARCH_TEXT:
            return {
                ...state,
                searchText: payload,
                searchItems: payload ? state.searchItems : [],
                searchPageCount: payload ? state.searchPageCount : 0,
                searchItemsPage: payload ? state.searchItemsPage : 0
            };
        case SET_SEARCH_DISPLAY_MODE:
            return {
                ...state,
                displayMode: payload
            };
        case SET_SEARCH_PAGE:
            return {
                ...state,
                searchItemsPage: payload.betweenOrBorder(1, state.searchPageCount)
            };
        default:
            return state
    }
}