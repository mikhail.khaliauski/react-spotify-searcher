import {routerReducer as routing} from "react-router-redux";
import error from 'reducers/errorReducer';
import authentication from "reducers/authenticationReducer";
import search from "reducers/searchReducer";
import globalPlayer from "reducers/globalPlayerReducer";
import player from "reducers/playerReducer";

import {combineReducers} from 'redux';

export default combineReducers({
    routing,
    error,
    authentication,
    search,
    globalPlayer,
    player,
});
