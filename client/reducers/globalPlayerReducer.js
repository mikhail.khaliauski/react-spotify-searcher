import {
    BEGIN_PAUSE_USER_PLAYTRACK,
    BEGIN_START_USER_PLAYTRACK,
    FAIL_PAUSE_USER_PLAYTRACK,
    FAIL_START_USER_PLAYTRACK,
    SUCCESS_START_USER_PLAYTRACK,
    SUCCESS_PAUSE_USER_PLAYTRACK,
    BEGIN_GET_USER_PLAYTRACK,
    SUCCESS_GET_USER_PLAYTRACK,
    FAIL_GET_USER_PLAYTRACK,

} from "actions/playActions";


const initialState = {
    isLoading: false,
    isPlaying: false,
    track: null
};

export default (state = initialState, action) => {
    const {type, payload} = action;
    switch (type) {
        case BEGIN_START_USER_PLAYTRACK:
            return {
                ...state,
                isPlaying: false,
                isLoading: true,
            };
        case SUCCESS_START_USER_PLAYTRACK:
            return {
                ...state,
                isLoading: false,
                isPlaying: true,
                track: payload
            };
        case FAIL_START_USER_PLAYTRACK:
            return initialState;
        case BEGIN_PAUSE_USER_PLAYTRACK:
            return {
                ...state,
                isLoading: true,
            };
        case SUCCESS_PAUSE_USER_PLAYTRACK:
            return {
                ...state,
                isLoading: false,
                isPlaying: false,
            };
        case FAIL_PAUSE_USER_PLAYTRACK:
            return initialState;
        case BEGIN_GET_USER_PLAYTRACK:
            return {
                ...state,
                isLoading: true
            };
        case SUCCESS_GET_USER_PLAYTRACK:
            return {
                ...state,
                isLoading: false,
                isPlaying: payload.isPlaying,
                track: payload.track
            };
        case FAIL_GET_USER_PLAYTRACK:
            return initialState;
        default:
            return state
    }
}