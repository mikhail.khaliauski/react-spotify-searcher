import {
    PLAY_PREVIEW,
    PAUSE_PREVIEW,
    CLEAR_PREVIEW
} from "actions/playActions";

const initialState = {
    isPlaying: false,
    track: null
};

export default (state = initialState, action) => {
    const {type, payload} = action;
    switch (type) {
        case PLAY_PREVIEW:
            return {
                ...state,
                isPlaying: true,
                track: payload || state.track
            };
        case PAUSE_PREVIEW:
            return {
                ...state,
                isPlaying: false
            };
        case CLEAR_PREVIEW:
            return initialState;
        default:
            return state
    }
}