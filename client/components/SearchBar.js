import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {updateSearchText, setSearchDisplayMode} from "../actions/searchActions";
import DisplayMode from "components/DisplayMode";
import {
    DISPLAY_MODE_GRID,
    DISPLAY_MODE_GRID_ICON,
    DISPLAY_MODE_LIST,
    DISPLAY_MODE_LIST_ICON,
} from "utils/constants";

class SearchBar extends PureComponent {
    static propTypes = {
        searchText: PropTypes.string.isRequired,
        updateSearchText: PropTypes.func.isRequired,
        displayMode: PropTypes.string.isRequired,
        switchDisplayMode: PropTypes.func.isRequired
    };
    static defaultProps = {};

    constructor() {
        super();
    }

    // noinspection JSUnusedGlobalSymbols
    componentWillMount = () => {
        this.buttonSet = [
            {
                key: DISPLAY_MODE_GRID,
                icon: DISPLAY_MODE_GRID_ICON,
                handleClick: this.props.switchDisplayMode.bind(this, DISPLAY_MODE_GRID)
            },
            {
                key: DISPLAY_MODE_LIST,
                icon: DISPLAY_MODE_LIST_ICON,
                handleClick: this.props.switchDisplayMode.bind(this, DISPLAY_MODE_LIST)
            }
        ];
    };

    handleChange = (event) => {
        this.props.updateSearchText(event.target.value);
    };


    render = () => {
        return (
            <div className="search search-bar">

                <div className="form">
                    <div className="row">
                        <div className="col-md-10">
                            <div className="form-group">
                                <input className="form-control search search-input" value={this.props.searchText}
                                       onChange={this.handleChange}/>
                            </div>
                        </div>
                        <div className="col-md-2">
                            <DisplayMode buttonSet={this.buttonSet} activeKey={this.props.displayMode}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (rState) => ({
    searchText: rState.search.searchText,
    displayMode: rState.search.displayMode
});

const mapDispatchToProps = {
    updateSearchText,
    switchDisplayMode: setSearchDisplayMode
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);