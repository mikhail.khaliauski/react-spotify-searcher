import React, {PureComponent} from 'react';
import {connect} from "react-redux";
import TrackSearchGridItem from "components/TrackSearchGridItem";

class SearchGrid extends PureComponent {
    static propTypes = {};
    static defaultProps = {};

    constructor() {
        super();
        this.state = {};
    }

    render() {
        return (
            <div className="search search-grid">
                {
                    this.props.items.length > 0 &&
                    this.props.items.map(item => {
                        return <TrackSearchGridItem key={item.id} item={item}/>
                    })
                }
            </div>
        )
    }
}


const mapStateToProps = (rState) => ({
    searchText: rState.search.searchText,
    items: rState.search.searchItems,
});

export default connect(mapStateToProps)(SearchGrid);