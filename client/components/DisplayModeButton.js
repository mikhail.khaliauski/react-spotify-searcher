import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Icon from 'react-fontawesome';

class DisplayModeButton extends PureComponent {
    static propTypes = {
        handleClick: PropTypes.func.isRequired,
        isActive: PropTypes.bool,
        icon: PropTypes.string
    };
    static defaultProps = {
        isActive: false,
        icon: "circle"
    };

    constructor() {
        super();
    }

    render() {
        return (
            <React.Fragment>
                <label className={classNames("btn btn-light  display-mode-button", {active: this.props.isActive})}
                       onClick={this.props.handleClick}>
                    <input readOnly={true} type={"radio"} name={this.props.scope} id={this.props.index}
                           autoComplete={"off"}
                           checked={this.props.isActive} value={this.props.index}/>
                    <Icon name={this.props.icon}/>
                </label>
            </React.Fragment>
        )
    }
}

export default DisplayModeButton;