import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {connect} from 'react-redux';

import IconButton from "components/IconButton";

import {beginGetUserPlaytrack, pausePreview, playPreview} from "actions/playActions";

class PlayingButtons extends PureComponent {
    static propTypes = {
        getUserPlayTrack: PropTypes.func.isRequired,
        onPlay: PropTypes.func.isRequired,
        onPause: PropTypes.func.isRequired,
        isPlaying: PropTypes.bool,
    };
    static defaultProps = {
        isPlaying: false
    };

    constructor() {
        super();
        this.state = {};
    }

    render() {

        return (
            <div className="play playing-buttons-wrapper">
                <IconButton onClick={this.props.getUserPlayTrack} classNames={"play playing-button btn btn-light"}
                            icon="refresh"/>
                {
                    this.props.isPlaying ?
                    <IconButton onClick={this.props.onPause} classNames={"play playing-button btn btn-dark"} icon="music"/> :
                    <IconButton onClick={this.props.onPlay} classNames={"play playing-button btn btn-light"} icon="music"/>
                }
            </div>
        )
    }
}


export default PlayingButtons;