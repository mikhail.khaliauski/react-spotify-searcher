import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-fontawesome';

class TopBarButton extends PureComponent {
    static propTypes = {
        icon: PropTypes.string.isRequired,
        navigate: PropTypes.string.isRequired
    };
    static defaultProps = {};

    constructor() {
        super();
    }

    render() {
        return (
            <a className={"btn btn-light topbar-navigation navigation-item"} href={this.props.navigate}>
                <Icon name={this.props.icon}/>
            </a>
        )
    }
}

export default TopBarButton