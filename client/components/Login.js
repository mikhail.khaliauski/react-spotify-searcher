import React, {PureComponent} from 'react';

import {URL_API_LOGIN} from "../api/authenticationApi";

class Login extends PureComponent {
    static propTypes = {};
    static defaultProps = {};

    constructor() {
        super();
    }

    render() {
        return (
            <div className={"w-100 h-100 d-flex justify-content-center"}>
                <a href={URL_API_LOGIN} className={"btn btn-lg btn-light"}>Login using Spotify</a>
            </div>
        )
    }
}


export default Login