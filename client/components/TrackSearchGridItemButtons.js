import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import IconButton from "components/IconButton";
import classNames from "classnames";

class TrackSearchGridItemButtons extends PureComponent {
    static propTypes = {
        onToggleGlobalPlay: PropTypes.func.isRequired,
        onPlay: PropTypes.func,
        onPause: PropTypes.func,
        isGlobalPlaying: PropTypes.bool,
        isLocalPlaying: PropTypes.bool,
        canBePreviewed: PropTypes.bool,
        canBeGlobalPlayed: PropTypes.bool
    };
    static defaultProps = {
        canBeGlobalPlayed: false,
        isGlobalPlaying: false,
        isLocalPlaying: false,
        canBePreviewed: true
    };

    constructor() {
        super();
        this.state = {};
    }

    render() {
        const globalPlayerButtonClasses = classNames("btn search search-grid-item-button", {
            "btn-light": !this.props.isGlobalPlaying,
            "btn-dark": this.props.isGlobalPlaying
        });
        let localPlayerButtonClasses = "btn btn-light search search-grid-item-button";

        return (
            <div className="search search-grid-item-buttons">
                {
                    this.props.canBePreviewed &&
                    (this.props.isLocalPlaying ?
                        <IconButton classNames={localPlayerButtonClasses} icon={"pause-circle"} isActive={false}
                                    onClick={this.props.onPause}/> :
                        <IconButton classNames={localPlayerButtonClasses} icon={"play-circle"} isActive={false}
                                    onClick={this.props.onPlay}/>)
                }
                {
                    this.props.canBeGlobalPlayed &&
                        <IconButton classNames={globalPlayerButtonClasses} icon={"music"}
                                    isActive={this.props.isGlobalPlaying}
                                    onClick={this.props.onToggleGlobalPlay}/>
                }
            </div>
        )
    }
}


export default TrackSearchGridItemButtons;