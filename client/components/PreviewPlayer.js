import React, {PureComponent} from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import {connect} from "react-redux";

import {
    playPreview,
    pausePreview,
    clearPreview
} from "actions/playActions";
import IconButton from "components/IconButton";

class PreviewPlayer extends PureComponent {
    static propTypes = {
        previewUrl: PropTypes.string.isRequired,
        onPlay: PropTypes.func.isRequired,
        onPause: PropTypes.func.isRequired,
        onClose: PropTypes.func.isRequired,
        isPlaying: PropTypes.bool
    };
    static defaultProps = {
        isPlaying: true
    };

    constructor() {
        super();
        this.state = {};
    }

    handlePlay = () => {
        this.props.onPlay();
    };

    // noinspection JSUnusedGlobalSymbols
    componentWillReceiveProps = (nextProps) => {
        if (nextProps.isPlaying === this.props.isPlaying) {
            return;
        }

        //if play started
        if (nextProps.isPlaying) {
            this.audio.play();
        } else {
            this.audio.pause();
        }
    };

    // noinspection JSUnusedGlobalSymbols
    componentWillUnmount = () => {
        this.props.onClose();
    };

    render() {
        return ReactDOM.createPortal(
            <div className="local-audio-player-wrapper">
                <audio ref={i => this.audio = i}
                       className="local-audio-player"
                       src={this.props.previewUrl}
                       onPlay={this.handlePlay}
                       onPause={this.props.onPause}
                    //onAbort={this.props.onAbort}
                       controls
                       autoPlay={true}
                >Audio player not working
                </audio>
                <IconButton icon={"close"} onClick={this.props.onClose}
                            classNames={"btn btn-link preview-player-close-button"}/>
            </div>,
            document.getElementsByTagName("body")[0]
        )
    }
}

const mapStateToProps = (rState) => ({
    previewUrl: rState.player.track.previewUrl,
    isPlaying: rState.player.isPlaying
});

const mapDispatchToProps = {
    onPlay: playPreview,
    onPause: pausePreview,
    onClose: clearPreview
};

export default connect(mapStateToProps, mapDispatchToProps)(PreviewPlayer)