import React, {PureComponent} from 'react';

class Spinner extends PureComponent {
    render() {
        return (
            <h6>
                Loading
            </h6>
        )
    }
}

export default Spinner