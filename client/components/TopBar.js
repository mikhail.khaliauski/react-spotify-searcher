import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import TopBarButton from "components/TopBarButton"
import {URL_API_LOGOUT} from "../api/authenticationApi";

class TopBar extends PureComponent {
    static propTypes = {
        title: PropTypes.string.isRequired,
        isAuthenticated: PropTypes.bool
    };
    static defaultProps = {
        isAuthenticated: false
    };

    render() {
        return (
            <div className={"topbar-navigation topbar"}>
                <label className={"topbar-navigation navigation-title"}>{this.props.title}</label>
                {this.props.isAuthenticated &&
                <div className={"topbar-navigation navigation-bar"}>
                    <TopBarButton icon={"user"} navigate={this.props.profileUrl}/>
                    <TopBarButton icon={"sign-out"} navigate={URL_API_LOGOUT}/>
                </div>
                }
            </div>
        )
    }
}

const mapStateToProps = (rState) => ({
    isAuthenticated: !!rState.authentication.user,
    profileUrl: rState.authentication.user && rState.authentication.user.profileUrl
});

export default connect(mapStateToProps)(TopBar)