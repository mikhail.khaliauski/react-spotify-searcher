import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {ToastMessageAnimated} from "react-toastr";
import {removeError} from "actions/errorActions";

class Error extends PureComponent {
    static propTypes = {
        errors: PropTypes.array.isRequired,
        removeError: PropTypes.func.isRequired
    };
    static defaultProps = {};

    constructor() {
        super();
        this.state = {};
    }


    onRemoveError = (id) => {
        this.props.removeError(id);
    };

    render() {
        return (
            <div className="toast-top-right" id="toast-container">
                {
                    this.props.errors.map(error => (
                        <ToastMessageAnimated key={error.id} type="error" timeOut={1500} onRemove={this.onRemoveError.bind(this, error.id)} message={error.message} />
                    ))
                }
            </div>
        )
    }
}

const mapStateToProps = (rState) => ({
    errors: rState.error
});

const mapDispatchToProps = {
    removeError
};

export default connect(mapStateToProps, mapDispatchToProps)(Error);