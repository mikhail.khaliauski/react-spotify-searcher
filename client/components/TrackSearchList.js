import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import TrackSearchListItem from "components/TrackSearchListItem";
import {connect} from "react-redux";

import {trackShape} from "utils/shapes";

class TrackSearchList extends PureComponent {
    static propTypes = {
        items: PropTypes.arrayOf(trackShape),
        searchText: PropTypes.string,
    };
    static defaultProps = {
        items: [],
        searchText: "",
        total: 0
    };

    constructor() {
        super();
    }

    render() {
        return (
            <div className="search search-list">
                {
                    this.props.items.length > 0 &&
                    this.props.items.map(item => {
                        return <TrackSearchListItem key={item.id} item={item}/>
                    })
                }
            </div>
        )
    }
}

const mapStateToProps = (rState) => ({
    searchText: rState.search.searchText,
    items: rState.search.searchItems,
});

export default connect(mapStateToProps)(TrackSearchList);