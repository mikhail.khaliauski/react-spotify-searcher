import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import classNames from "classnames";
import ReactDOMServer from "react-dom/server";
import {createUltimatePagination, ITEM_TYPES} from 'react-ultimate-pagination';
import Icon from "react-fontawesome";

import {beginSearch, setSearchPage} from "actions/searchActions";


//<editor-fold desc="react-ultimate-pagination initialization">

const withPreventDefault = (fn) => (event) => {
    event.preventDefault();
    fn && fn();
};

const WrapperComponent = ({children}) => (
    <div className="btn-group" role="group" aria-label="pagination">{children}</div>
);

const PaginationItem = ({value, isActive, onClick}) => (
    <a className={classNames("btn btn btn-light", {"active": isActive})} href="#"
       onClick={withPreventDefault(onClick)}>{value}</a>
);

const PaginationItemMobile = ({value, onClick, ...props}) => (
    <a href="#" className={classNames("btn btn btn-outline-secondary")}
       onClick={withPreventDefault(onClick)} {...props} >{value}</a>
);

const Page = ({value, isActive, onClick}) => <PaginationItem value={value} isActive={isActive} onClick={onClick}/>;
const Ellipsis = ({onClick}) => <PaginationItem value={"..."} onClick={onClick}/>;
const FirstPageLink = ({onClick}) => <PaginationItem value={"«"} onClick={onClick}/>;
const PreviousPageLink = ({onClick}) => <PaginationItem value={"‹"} onClick={onClick}/>;
const NextPageLink = ({onClick}) => <PaginationItem value={"›"} onClick={onClick}/>;
const LastPageLink = ({onClick}) => <PaginationItem value={"»"} onClick={onClick}/>;


class PageMobile extends PureComponent {

    static propTypes = {
        value: PropTypes.number,
        isActive: PropTypes.bool,
        onClick: PropTypes.func,
    };

    //Use context since there's no api to pass additional props through lib components
    static contextTypes = {
        totalPages: PropTypes.number
    };

    constructor() {
        super();
        this.state = {};
    }

    // noinspection JSUnusedGlobalSymbols
    componentDidMount = () => {
        this.setState({
            value: this.props.value
        })
    };

    render = () => (
        <PaginationItemMobile
            id="track-results-pagination-mobile-popover-container"
            value={`${this.props.value} of ${this.context.totalPages}`}
        />
    )
}

const PreviousPageLinkMobile = ({onClick}) => <PaginationItemMobile value={"‹"} onClick={onClick}/>;
const NextPageLinkMobile = ({onClick}) => <PaginationItemMobile value={"›"} onClick={onClick}/>;

const None = () => <span className={"track-results-pagination-none"}/>;


const paginationConfiguration = {
    [ITEM_TYPES.PAGE]: Page,
    [ITEM_TYPES.ELLIPSIS]: Ellipsis,
    [ITEM_TYPES.FIRST_PAGE_LINK]: FirstPageLink,
    [ITEM_TYPES.PREVIOUS_PAGE_LINK]: PreviousPageLink,
    [ITEM_TYPES.NEXT_PAGE_LINK]: NextPageLink,
    [ITEM_TYPES.LAST_PAGE_LINK]: LastPageLink
};

const paginationConfigurationMobile = {
    [ITEM_TYPES.PAGE]: PageMobile,
    [ITEM_TYPES.ELLIPSIS]: None,
    [ITEM_TYPES.FIRST_PAGE_LINK]: None,
    [ITEM_TYPES.PREVIOUS_PAGE_LINK]: PreviousPageLinkMobile,
    [ITEM_TYPES.NEXT_PAGE_LINK]: NextPageLinkMobile,
    [ITEM_TYPES.LAST_PAGE_LINK]: None

};

const Pagination = createUltimatePagination({
    itemTypeToComponent: paginationConfiguration,
    WrapperComponent
});

const PaginationMobile = createUltimatePagination({
    itemTypeToComponent: paginationConfigurationMobile,
    WrapperComponent
});

//</editor-fold>

class TrackSearchPagination extends PureComponent {
    static propTypes = {
        currentPage: PropTypes.number.isRequired,
        totalPages: PropTypes.number.isRequired,
        setSearchPage: PropTypes.func.isRequired,
        searchText: PropTypes.string,
    };
    static defaultProps = {
        searchText: ""
    };
    static childContextTypes = {
        totalPages: PropTypes.number
    };

    getChildContext = () => ({
        totalPages: this.props.totalPages
    });

    constructor() {
        super();
        this.state = {};
    }

    // noinspection JSUnusedGlobalSymbols
    componentWillReceiveProps = (nextProps) => {
        if (nextProps.currentPage !== this.props.currentPage) {
            this.props.search()
        }
    };

    render() {
        return (
            <React.Fragment>
                <div className="track-results-pagination d-flex d-md-none">
                    <nav aria-label="Tracks search result pages">
                        <PaginationMobile
                            currentPage={this.props.currentPage}
                            totalPages={this.props.totalPages}
                            boundaryPagesRange={0}
                            siblingPagesRange={0}
                            hideEllipses={true}
                            hideFirstAndLastPageLinks={true}
                            onChange={this.props.setSearchPage}
                        />
                    </nav>
                </div>
                <div className="track-results-pagination d-none d-md-flex">
                    <nav aria-label="Tracks search result pages">
                        <Pagination
                            currentPage={this.props.currentPage}
                            totalPages={this.props.totalPages}
                            siblingPagesRange={5}
                            onChange={this.props.setSearchPage}
                        />
                    </nav>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (rState) => ({
    currentPage: rState.search.searchItemsPage,
    totalPages: rState.search.searchPageCount,
    searchText: rState.search.searchText
});

const mapDispatchToProps = {
    search: beginSearch,
    setSearchPage
};

export default connect(mapStateToProps, mapDispatchToProps)(TrackSearchPagination)