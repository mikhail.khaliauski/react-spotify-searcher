import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import SearchBar from "components/SearchBar";
import TrackSearchList from "components/TrackSearchList";
import TrackSearchGrid from "components/TrackSearchGrid";
import Spinner from "components/Spinner";
import Pagination from "components/TrackSearchPagination";
import PreviewPlayer from "components/PreviewPlayer";

import {beginSearch, clearSearch, setSearchPage} from "actions/searchActions";

import {DISPLAY_MODE_GRID, DISPLAY_MODE_LIST} from "utils/constants";
import {beginGetUserPlaytrack} from "actions/playActions";

class Search extends PureComponent {
    static propTypes = {
        searchText: PropTypes.string.isRequired,
        total: PropTypes.number.isRequired,
        displayMode: PropTypes.string.isRequired,
        search: PropTypes.func.isRequired,
        getUserPlayTrack: PropTypes.func.isRequired,
        isLoading: PropTypes.bool,
        shouldDisplayPagination: PropTypes.bool,
        shouldDisplayPlayer: PropTypes.bool
    };
    static defaultProps = {
        isLoading: false,
        shouldDisplayPagination: false,
        shouldDisplayPlayer: false
    };

    constructor() {
        super();
    }

    // noinspection JSUnusedGlobalSymbols
    componentDidMount = () => {
        this.getUserPlaytrackDaemon = setInterval(this.props.getUserPlayTrack, 4000);
    };

    // noinspection JSUnusedGlobalSymbols
    componentWillUnmount = () => {
        clearInterval(this.getUserPlaytrackDaemon);
    };

    // noinspection JSUnusedGlobalSymbols
    componentWillReceiveProps = (nextProps) => {

        if (!nextProps.searchText) {
            this.props.clearSearch();
            return;
        }

        if (nextProps.searchText !== this.props.searchText) {
            this.props.resetPage();
            this.props.search();
        }
    };

    render() {
        return (
            <React.Fragment>
                <div className="row">
                    <div className="col">
                        <SearchBar/>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        {
                            this.props.searchText && !this.props.isLoading &&
                            <h5 className="search search-total">{this.props.total || "No"} tracks found</h5>
                        }
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        <div className="search search-results">
                            {
                                this.props.isLoading &&
                                <Spinner/>
                            }

                            {
                                this.props.displayMode === DISPLAY_MODE_GRID &&
                                <TrackSearchGrid/>
                            }
                            {
                                this.props.displayMode === DISPLAY_MODE_LIST &&
                                <TrackSearchList/>
                            }
                        </div>
                    </div>
                </div>
                {
                    this.props.shouldDisplayPagination &&
                    <div className="row mt-2">
                        <div className="col">
                            <Pagination/>
                        </div>
                    </div>
                }
                {
                    this.props.shouldDisplayPlayer &&
                    <PreviewPlayer/>
                }


            </React.Fragment>
        )
    }
}

const mapStateToProps = (rState) => ({
    isLoading: rState.search.isLoading,
    searchText: rState.search.searchText,
    total: rState.search.searchItemsCount,
    displayMode: rState.search.displayMode,
    shouldDisplayPagination: rState.search.searchPageCount > 1,
    shouldDisplayPlayer: rState.player.track && !!rState.player.track.previewUrl
});

const mapDispatchToProps = {
    search: beginSearch,
    resetPage: setSearchPage.bind(this, 1),
    clearSearch,
    getUserPlayTrack: beginGetUserPlaytrack
};

export default connect(mapStateToProps, mapDispatchToProps)(Search)