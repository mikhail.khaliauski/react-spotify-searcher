import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {connect} from 'react-redux';

import PlayingButtons from "components/PlayingButtons";
import IconButton from "components/IconButton";
import Icon from "react-fontawesome";

import {
    beginGetUserPlaytrack, beginPauseUserPlayTrack, beginSwitchUserPlaytrackToPrevious,
    beginStartUserPlayTrack, beginSwitchUserPlaytrackToNext
} from "actions/playActions";

import {trackShape} from "utils/shapes";

class PlayingTrack extends PureComponent {
    static propTypes = {
        isAllowed: PropTypes.bool.isRequired,
        isPlaying: PropTypes.bool.isRequired,
        getUserPlayTrack: PropTypes.func.isRequired,
        switchUserPlayTrackToPrevious: PropTypes.func.isRequired,
        switchUserPlayTrackToNext: PropTypes.func.isRequired,
        playGlobal: PropTypes.func.isRequired,
        pauseGlobal: PropTypes.func.isRequired,
        isLoading: PropTypes.bool,
        track: trackShape,
    };
    static defaultProps = {
        isLoading: false
    };

    constructor() {
        super();
        this.state = {};
    }

    // noinspection JSUnusedGlobalSymbols
    componentDidMount = () => {
        this.props.getUserPlayTrack();
    };

    handleTrackStart = () => {
        if (this.props.track) {
            this.props.playGlobal(this.props.track);
        }
    };

    handleTrackPause = () => {
        if (this.props.isPlaying) {
            this.props.pauseGlobal();
        }
    };

    render() {
        return (

            <div className="play playing-track-wrapper">
                {
                    !this.props.isAllowed &&
                    <h2>Premium users only</h2>
                }
                {
                    this.props.isAllowed &&
                    this.props.isLoading &&
                    <span>Loading...</span>
                }
                {
                    this.props.isAllowed &&
                    !this.props.isLoading &&
                    !this.props.track &&
                    <h2> No playing track found</h2>
                }
                {
                    this.props.isAllowed &&
                    this.props.track &&
                    <React.Fragment>
                        <div className="play playing-track">
                            <div className="play playing-track-button">
                                <IconButton onClick={this.props.switchUserPlayTrackToPrevious} classNames="btn btn-block btn-light" icon={"chevron-left"}/>
                            </div>
                            <div className="play playing-track-image-wrapper">
                                <img className={classNames("play playing-track-image", {"playing": this.props.isPlaying})} src={this.props.track.imageUrl}
                                     alt="Track Image"/>
                            </div>
                            <div className="play playing-track-button">
                                <IconButton onClick={this.props.switchUserPlayTrackToNext} classNames="btn btn-block btn-light" icon={"chevron-right"}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col d-flex justify-content-end">
                                <PlayingButtons isPlaying={this.props.isPlaying} getUserPlayTrack={this.props.getUserPlayTrack} onPlay={this.handleTrackStart} onPause={this.handleTrackPause}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col">
                                <div className="play playing-description-wrapper">
                                   <div className="row">
                                       <div className="col-1">
                                           <Icon name="users"/>
                                       </div>
                                       <div className="col-10">
                                           {this.props.track.artists}
                                       </div>
                                   </div>
                                   <div className="row">
                                       <div className="col-1">
                                           <Icon name="book"/>
                                       </div>
                                       <div className="col-10">
                                           {this.props.track.album}
                                       </div>
                                   </div>
                                   <div className="row">
                                       <div className="col-1">
                                           <Icon name="music"/>
                                       </div>
                                       <div className="col-10">
                                           {this.props.track.name}
                                       </div>
                                   </div>
                                </div>

                            </div>
                        </div>
                    </React.Fragment>
                }
            </div>
        )
    }
}

const mapStateToProps = (rState) => ({
    isAllowed: rState.authentication.isPremium,
    isLoading: rState.globalPlayer.isLoading,
    track: rState.globalPlayer.track,
    isPlaying: !!rState.globalPlayer.track && rState.globalPlayer.isPlaying,

});

const mapDispatchToProps = {
    getUserPlayTrack: beginGetUserPlaytrack,
    playGlobal: beginStartUserPlayTrack,
    pauseGlobal: beginPauseUserPlayTrack,
    switchUserPlayTrackToPrevious: beginSwitchUserPlaytrackToPrevious,
    switchUserPlayTrackToNext: beginSwitchUserPlaytrackToNext,
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayingTrack);