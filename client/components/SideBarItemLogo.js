import React, {Component} from 'react';
import SidebarItem from "components/SidebarItem";
import {logo} from "utils/sidebarRoutingUtils";

class SideBarItemLogo extends Component {
    static propTypes = {};
    static defaultProps = {
        item: {
            icon: "circle-o"
        },
    };

    constructor() {
        super();
    }

    render() {
        return (
            <SidebarItem classNames={["navigation-link-logo"]} item={logo}/>
        )
    }
}

export default SideBarItemLogo