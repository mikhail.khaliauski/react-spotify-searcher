import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import PropTypes from "prop-types";
import classNames from "classnames";
import ReactDOMServer from "react-dom/server";

import TrackSearchListItemButtons from "components/TrackSearchListItemButtons";
import Icon from "react-fontawesome";

import {trackShape} from "utils/shapes";

import {beginPauseUserPlayTrack, beginStartUserPlayTrack, pausePreview, playPreview} from "actions/playActions";

class TrackSearchListItem extends PureComponent {
    static propTypes = {
        item: trackShape.isRequired,
        playLocal: PropTypes.func.isRequired,
        pauseLocal: PropTypes.func.isRequired,
        playGlobal: PropTypes.func.isRequired,
        pauseGlobal: PropTypes.func.isRequired,
        isUserPremium: PropTypes.bool,
        isLocal: PropTypes.bool,
        isLocalPlaying: PropTypes.bool,
        isGlobal: PropTypes.bool,
        isGlobalPlaying: PropTypes.bool,
    };
    static defaultProps = {
        isUserPremium: false,
        isLocal: false,
        isLocalPlaying: false,
        isGlobal: false,
        isGlobalPlaying: false
    };

    constructor() {
        super();
        this.state = {};
    }

    // noinspection JSUnusedGlobalSymbols
    componentDidMount = () => {
        (($) => {
            $("[data-toggle=\"search-list-item-popover\"]").popover({
                template: this.getPopoverTemplate(),
                trigger: "click",
                html: true,
                placement: "top",
            })
        })(jQuery)
    };

    getPopoverTemplate = () => {
        return ReactDOMServer.renderToStaticMarkup(
            <div className="popover" role="tooltip">
                <div className="arrow"/>
                <div className="popover-body p-0">
                    <div className="popover-content "/>
                </div>
            </div>
        );
    };

    getPopoverContent = () => {
        return ReactDOMServer.renderToStaticMarkup(
            <div className={"search search-list-item-popover"}>
                <img className={"search search-list-item-popover-image"} src={this.props.item.imageUrl}/>
                <div className={"search search-list-item-popover-content"}>
                    <div title={this.props.item.artists} className="search search-list-item-popover-text">
                        <Icon className="mr-2" name="users"/>
                        {this.props.item.artists}
                    </div>
                    <div title={this.props.item.album} className="search search-list-item-popover-text">
                        <Icon className="mr-2" name="book"/>
                        {this.props.item.album}
                    </div>
                    <div title={this.props.item.name} className="search search-list-item-popover-text">
                        <Icon className="mr-2" name="music"/>
                        {this.props.item.name}
                    </div>
                </div>
            </div>
        )
    };

    onTrackPlay = () => {
        if (!this.props.isLocal) {
            this.props.playLocal(this.props.item);
        }
        this.props.playLocal();
    };

    onTrackPause = () => {
        if (this.props.isLocal && this.props.isLocalPlaying) {
            this.props.pauseLocal(this.props.item);
        }
    };

    onTrackToggleGlobalPlay = () => {
        if (this.props.isGlobal && this.props.isGlobalPlaying) {
            this.props.pauseGlobal();
        } else {
            this.props.playGlobal(this.props.item);
        }
    };

    render() {
        let canBePreviewed = !!this.props.item.previewUrl;
        let canBeGlobalPlayed = this.props.isUserPremium;


        const isGlobalAndPlaying = this.props.isGlobal && this.props.isGlobalPlaying;

        const listItemClasses = classNames("card search search-list-item bg-light", {
            "playing": isGlobalAndPlaying
        });

        return (
            <div className={listItemClasses}>
                <div className="card-block">
                    <div className="media">
                        <img data-toggle="search-list-item-popover"
                             data-content={this.getPopoverContent(this.props.item)} src={this.props.item.imageUrl}
                             alt="No Image"
                             className="d-flex mr-3 align-self-center search search-list-item-image"/>
                        <div className="media-body">
                            <span className={"search search-list-item-title"}>{this.props.item.name}</span>
                        </div>
                        <div className="d-flex align-self-center">
                            <TrackSearchListItemButtons
                                isGlobalPlaying={this.props.isGlobal && this.props.isGlobalPlaying}
                                isLocalPlaying={this.props.isLocal && this.props.isLocalPlaying}
                                canBePreviewed={canBePreviewed}
                                canBeGlobalPlayed={canBeGlobalPlayed}
                                onPlay={this.onTrackPlay} onPause={this.onTrackPause}
                                onToggleGlobalPlay={this.onTrackToggleGlobalPlay}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (rState, ownProps) => ({
    isUserPremium: rState.authentication.isPremium,
    isGlobal: !!rState.globalPlayer.track && rState.globalPlayer.track.id === ownProps.item.id,
    isGlobalPlaying: !!rState.globalPlayer.track && rState.globalPlayer.isPlaying,
    isLocal: !!rState.player.track && rState.player.track.id === ownProps.item.id,
    isLocalPlaying: !!rState.player.track && rState.player.isPlaying,
});

const mapDispatchToProps = {
    playLocal: playPreview,
    pauseLocal: pausePreview,
    playGlobal: beginStartUserPlayTrack,
    pauseGlobal: beginPauseUserPlayTrack
};


export default connect(mapStateToProps, mapDispatchToProps)(TrackSearchListItem)