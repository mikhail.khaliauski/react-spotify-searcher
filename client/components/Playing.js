import React, {PureComponent} from 'react';

import PlayingTrack from "components/PlayingTrack";

class Playing extends PureComponent {
    static propTypes = {};
    static defaultProps = {};

    constructor() {
        super();
        this.state = {}
    }

    render() {
        return (
            <div className="play playing-container">

                <div className="row">
                    <div className="col d-flex justify-content-center">
                        <PlayingTrack/>
                    </div>
                </div>
                <div className="row">
                    <div className="col">

                    </div>
                </div>
            </div>
        )
    }
}

export default Playing