import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';
import Icon from 'react-fontawesome';
import classNames from 'classnames';

class SidebarItem extends Component {
    static propTypes = {
        item: PropTypes.shape({
            name: PropTypes.string.isRequired,
            url: PropTypes.string.isRequired,
            icon: PropTypes.string
        }),
        classNames: PropTypes.array
    };
    static defaultProps = {
        item: {
            icon: "circle-o"
        },
        classNames: ["navigation-link"]
    };

    constructor() {
        super();
    }

    render() {
        return (
            <NavLink to={this.props.item.url}
                     className={classNames("btn", "btn-light", "sidebar-navigation", this.props.classNames)}
                     activeClassName={"active"}
                     exact={this.props.exact}>
                <Icon className={"sidebar-navigation navigation-link-icon"} name={this.props.item.icon}/>
                <div className={"sidebar-navigation navigation-text"}>{this.props.item.name}</div>
            </NavLink>
        )
    }
}

export default SidebarItem