import React, {PureComponent} from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import Sidebar from "components/Sidebar"
import TopBar from "components/TopBar"
import Error from "components/Error"


import Search from 'components/Search'
import Playing from 'components/Playing'
import Playlists from 'components/Playlists'
import Login from "components/Login";


import {
    authorizedTabs, unauthorizedTabs, URL_LOGIN, URL_PLAY, URL_PLAYLISTS,
    URL_SEARCH, getNameByUrl, URL_HOME
} from "utils/sidebarRoutingUtils";

import {beginFetchUser} from "actions/authenticationActions";

class App extends PureComponent {
    static propTypes = {
        fetchUser: PropTypes.func.isRequired,
        isAuthenticated: PropTypes.bool,
    };
    static defaultProps = {
        isAuthenticated: false
    };

    // noinspection JSUnusedGlobalSymbols
    componentDidMount = () => {
        this.props.fetchUser();
    };

    render() {
        let navigationTabs = this.props.isAuthenticated ? authorizedTabs : unauthorizedTabs;
        return (
            <div className={"container-fluid"}>
                <div className={"row"}>
                    <div className={"col-xs-12 col-md-2"}>
                        <Sidebar navigationTabs={navigationTabs}/>
                    </div>
                    <div className={"col-xs-12 col-md-10"}>
                        <TopBar title={getNameByUrl(this.props.location.pathname)}/>
                        <div className={"content"}>
                            {this.props.isAuthenticated &&
                            <Switch>
                                <Redirect from={URL_HOME} exact={true} to={URL_SEARCH}/>
                                <Route path={URL_SEARCH} component={Search}/>
                                <Route path={URL_PLAY} component={Playing}/>
                                <Route path={URL_PLAYLISTS} component={Playlists}/>
                                <Redirect to={URL_HOME}/>
                            </Switch>
                            }
                            {!this.props.isAuthenticated &&
                            <Switch>
                                <Redirect from={URL_HOME} exact={true} to={URL_LOGIN}/>
                                <Route path={URL_LOGIN} component={Login}/>
                                <Redirect to={URL_HOME}/>
                            </Switch>
                            }
                        </div>
                    </div>
                </div>
                <Error/>
            </div>
        );
    }
}


const mapStateToProps = (rState) => ({
    isAuthenticated: !!rState.authentication.user,
});

const mapDispatchToProps = {
    fetchUser: beginFetchUser

};


export default connect(mapStateToProps, mapDispatchToProps)(App);