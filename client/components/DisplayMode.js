import React, {Component} from 'react';
import PropTypes from 'prop-types';

import DisplayModeButton from "components/DisplayModeButton";

class DisplayMode extends Component {
    static propTypes = {
        buttonSet: PropTypes.arrayOf(PropTypes.shape({
            key: PropTypes.string.isRequired,
            handleClick: PropTypes.func.isRequired,
            icon: PropTypes.string,
        })),
        scope: PropTypes.string,
        activeKey: PropTypes.string
    };
    static defaultProps = {
        buttonSet: [
            {
                icon: "cycle",
            }
        ],
        scope: Date.now().toString()
    };

    constructor() {
        super();
    }

    render() {
        return (
            <div className="btn-group btn-group-toggle display-mode" data-toggle="buttons">
                {this.props.buttonSet.map((item, i) => {

                    return (
                        <DisplayModeButton key={item.key} icon={item.icon}
                                           isActive={item.key === this.props.activeKey}
                                           handleClick={item.handleClick}
                                           index={i}/>
                    )
                })}
            </div>
        )
    }
}

export default DisplayMode