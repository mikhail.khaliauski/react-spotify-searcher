import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import {logo} from "../utils/sidebarRoutingUtils";

import SidebarItem from './SidebarItem'
import SideBarItemLogo from "components/SideBarItemLogo";

class Sidebar extends Component {
    static propTypes = {
        navigationTabs: PropTypes.arrayOf(PropTypes.shape({
            name: PropTypes.string.isRequired,
            url: PropTypes.string.isRequired,
            icon: PropTypes.string
        }))
    };
    static defaultProps = {
        navigationTabs: []
    };

    constructor() {
        super();
    }

    render() {
        let sidebarClass = classNames({
            "sidebar": true
        });
        return (
            <div className={sidebarClass}>
                {
                    <div className={"row"}>
                        <div className={"col"}>
                            <SideBarItemLogo item={logo}/>
                        </div>
                    </div>
                }
                <div className={"row"}>
                    <div className={"col"}>
                        <div className={"sidebar-navigation navigation-bar"}>
                            {
                                this.props.navigationTabs.map((item) => {
                                    return <SidebarItem key={item.name} location={this.props.location} item={item}/>
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Sidebar