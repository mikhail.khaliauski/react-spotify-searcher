import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Icon from "react-fontawesome";

class IconButton extends PureComponent {
    static propTypes = {
        icon: PropTypes.string.isRequired,
        onClick: PropTypes.func.isRequired,
        classNames: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.object
        ]),
        isActive: PropTypes.bool,
        disabled: PropTypes.bool,
    };
    static defaultProps = {
        classNames: {},
        isActive: false,
        disabled: false
    };

    constructor() {
        super();
        this.state = {};
    }

    render() {

        let {classNames: classes, isActive, ...other} = this.props;


        return (
            <button className={classNames(classes, {
                active: isActive
            })} {...other}>
                <Icon name={this.props.icon}/>
            </button>
        )
    }
}

export default IconButton