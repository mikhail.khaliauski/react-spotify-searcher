import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import {trackShape} from "utils/shapes";
import Icon from "react-fontawesome";
import {connect} from 'react-redux';

import TrackSearchGridItemButtons from "components/TrackSearchGridItemButtons";

import {playPreview, pausePreview, beginStartUserPlayTrack, beginPauseUserPlayTrack} from "actions/playActions";


class TrackSearchGridItem extends PureComponent {
    static propTypes = {
        item: trackShape.isRequired,
        playLocal: PropTypes.func.isRequired,
        pauseLocal: PropTypes.func.isRequired,
        playGlobal: PropTypes.func.isRequired,
        pauseGlobal: PropTypes.func.isRequired,
        isUserPremium: PropTypes.bool,
        isLocal: PropTypes.bool,
        isLocalPlaying: PropTypes.bool,
        isGlobal: PropTypes.bool,
        isGlobalPlaying: PropTypes.bool,
    };
    static defaultProps = {
        isUserPremium: false,
        isLocal: false,
        isLocalPlaying: false,
        isGlobal: false,
        isGlobalPlaying: false
    };

    constructor() {
        super();
        this.state = {};

    }

    onTrackPlay = () => {
        if (!this.props.isLocal) {
            this.props.playLocal(this.props.item);
        }
        this.props.playLocal();
    };

    onTrackToggleGlobalPlay = () => {
        if (this.props.isGlobal && this.props.isGlobalPlaying) {
            this.props.pauseGlobal();
        } else {
            this.props.playGlobal(this.props.item);
        }
    };

    onTrackPause = () => {
        if (this.props.isLocal && this.props.isLocalPlaying) {
            this.props.pauseLocal(this.props.item);
        }
    };

    render() {

        let canBePreviewed = !!this.props.item.previewUrl;
        let canBeGlobalPlayed = this.props.isUserPremium;

        return (
            <div className={classNames("search search-grid-item", {
                "playing": this.props.isGlobal && this.props.isGlobalPlaying
            })}>
                <img src={this.props.item.imageUrl} alt="" className="search search-grid-item-image"/>
                <div className="search search-grid-item-overlay">
                    <div className="search search-grid-item-overlay-content">
                        <div className="search search-grid-item-overlay-content-hovered">

                            <TrackSearchGridItemButtons
                                isGlobalPlaying={this.props.isGlobal && this.props.isGlobalPlaying}
                                isLocalPlaying={this.props.isLocal && this.props.isLocalPlaying}
                                canBePreviewed={canBePreviewed}
                                canBeGlobalPlayed={canBeGlobalPlayed}
                                onPlay={this.onTrackPlay} onPause={this.onTrackPause}
                                onToggleGlobalPlay={this.onTrackToggleGlobalPlay}/>
                        </div>
                        <div className="search search-grid-item-overlay-panel">
                            <div title={this.props.item.artists} className="search search-grid-item-text">
                                <Icon className="mr-2" name="users"/>
                                {this.props.item.artists}
                            </div>
                            <div title={this.props.item.album} className="search search-grid-item-text">
                                <Icon className="mr-2" name="book"/>
                                {this.props.item.album}
                            </div>
                            <div title={this.props.item.name} className="search search-grid-item-text">
                                <Icon className="mr-2" name="music"/>
                                {this.props.item.name}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

const mapStateToProps = (rState, ownProps) => ({
    isUserPremium: rState.authentication.isPremium,
    isGlobal: !!rState.globalPlayer.track && rState.globalPlayer.track.id === ownProps.item.id,
    isGlobalPlaying: !!rState.globalPlayer.track && rState.globalPlayer.isPlaying,
    isLocal: !!rState.player.track && rState.player.track.id === ownProps.item.id,
    isLocalPlaying: !!rState.player.track && rState.player.isPlaying,
});


const mapDispatchToProps = {
    playLocal: playPreview,
    pauseLocal: pausePreview,
    playGlobal: beginStartUserPlayTrack,
    pauseGlobal: beginPauseUserPlayTrack
};

export default connect(mapStateToProps, mapDispatchToProps)(TrackSearchGridItem);