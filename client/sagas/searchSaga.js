import {call, put, takeEvery, takeLatest, select} from 'redux-saga/effects';

import {
    BEGIN_SEARCH,
    successSearch,
    failSearch,
} from "actions/searchActions";

import SearchApi from "api/searchApi";
import {SEARCH_RESULTS_PER_PAGE} from "utils/constants";

function* makeSearch() {
    try {
        const {searchText, searchItemsPage} = yield select(state => state.search);
        const searchResults = yield call(SearchApi.search, searchText, (searchItemsPage - 1) * SEARCH_RESULTS_PER_PAGE);
        yield put(successSearch(searchResults))
    } catch (e) {
        yield put(failSearch())
    }
}

function* saga() {
    yield takeLatest(BEGIN_SEARCH, makeSearch)
}

export default saga;