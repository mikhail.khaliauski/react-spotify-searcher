import {call, put, fork, takeEvery, takeLatest, all} from 'redux-saga/effects';

import {
    BEGIN_FETCH_USER,
    LOGOUT,
    successFetchUser,
    logout,
} from "actions/authenticationActions";

import {clearSession, saveAccessToken} from "utils/sessionUtils";

import AuthenticationApi from "api/authenticationApi";

function* makeFetchUser() {
    try {
        const {accessToken, ...user} = yield call(AuthenticationApi.getUser);
        yield call(saveAccessToken, accessToken);
        yield put(successFetchUser(user));

    } catch (e) {
        yield put(logout())
    }
}

function* makeLogout() {
    yield call(clearSession);
}

function* fetchUserSaga() {
    yield takeEvery(BEGIN_FETCH_USER, makeFetchUser)
}

function* logoutSaga() {
    yield takeLatest(LOGOUT, makeLogout)
}

function* saga() {
    yield all([
        fork(fetchUserSaga),
        fork(logoutSaga)
    ])
}

export default saga;