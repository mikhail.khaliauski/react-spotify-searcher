import {fork, all} from "redux-saga/effects";

import searchSaga from "sagas/searchSaga";
import globalErrorHandlingSaga from "sagas/globalErrorHandlingSaga"
import authenticationSaga from "sagas/authenticationSaga"
import globalPlayerSaga from "sagas/globalPlayerSaga"

function* saga() {
    yield all([
        fork(searchSaga),
        fork(globalErrorHandlingSaga),
        fork(authenticationSaga),
        fork(globalPlayerSaga)
    ])
}

export default saga;