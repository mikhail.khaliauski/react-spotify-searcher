import {call, put, fork, all, takeEvery, takeLatest, select} from 'redux-saga/effects';

import {
    BEGIN_GET_USER_PLAYTRACK,
    BEGIN_PAUSE_USER_PLAYTRACK,
    BEGIN_SWITCH_USER_PLAYTRACK_TO_NEXT,
    BEGIN_SWITCH_USER_PLAYTRACK_TO_PREVIOUS,
    BEGIN_START_USER_PLAYTRACK,
    failGetUserPlaytrack,
    failPauseUserPlayTrack,
    failPlaytrackNext,
    failSwitchUserPlaytrackToPrevious,
    failStartUserPlayTrack,
    SUCCESS_SWITCH_USER_PLAYTRACK_TO_NEXT,
    SUCCESS_SWITCH_USER_PLAYTRACK_TO_PREVIOUS,
    successGetUserPlaytrack,
    successPauseUserPlayTrack,
    successPlaytrackNext,
    successSwitchUserPlaytrackToPrevious,
    successStartUserPlayTrack
} from "actions/playActions";

import PlayerApi from "api/playerApi";

function* makeStartResumeUserPlaytrack(action) {
    try {
        const {payload} = action;
        const stateTrack = yield select(state => state.globalPlayer.track);

        const isResuming = !!(stateTrack && stateTrack.id === action.payload.id);
        yield call(PlayerApi.playTrack, isResuming ? null : action.payload.id);
        yield put(successStartUserPlayTrack(action.payload))
    } catch (e) {
        yield put(failStartUserPlayTrack(e))
    }

}

function* makePauseUserPlaytrack() {
    try {
        yield call(PlayerApi.pauseTrack);
        yield put(successPauseUserPlayTrack())
    } catch (e) {
        yield put(failPauseUserPlayTrack())
    }
}

function* makePlaytrackNext() {
    try {
        yield call(PlayerApi.next);
        yield put(successPlaytrackNext())
    } catch (e) {
        yield put(failPlaytrackNext(e))
    }
}

function* makePlaytrackPrevious() {
    try {
        yield call(PlayerApi.previous);
        yield put(successSwitchUserPlaytrackToPrevious())
    } catch (e) {
        yield put(failSwitchUserPlaytrackToPrevious(e))
    }
}

function* makeGetUserPlaytrack() {
    try {
        const payload = yield call(PlayerApi.getPlaying);
        yield put(successGetUserPlaytrack(payload));
    } catch (e) {
        yield put(failGetUserPlaytrack());
    }
}

function* startResumeUserPlaytrackSaga() {
    yield takeEvery(BEGIN_START_USER_PLAYTRACK, makeStartResumeUserPlaytrack)
}

function* pauseUserlPlaytrackSaga() {
    yield takeEvery(BEGIN_PAUSE_USER_PLAYTRACK, makePauseUserPlaytrack)
}

function* getUserPlaytrackSaga() {
    yield takeEvery(BEGIN_GET_USER_PLAYTRACK, makeGetUserPlaytrack)
}

function* playtrackNextSaga() {
    yield takeEvery(BEGIN_SWITCH_USER_PLAYTRACK_TO_NEXT, makePlaytrackNext)
}

function* playtrackPreviousSaga() {
    yield takeEvery(BEGIN_SWITCH_USER_PLAYTRACK_TO_PREVIOUS, makePlaytrackPrevious)
}

function* updateOnSwitchPlaytrackSaga() {
    yield takeLatest([SUCCESS_SWITCH_USER_PLAYTRACK_TO_NEXT, SUCCESS_SWITCH_USER_PLAYTRACK_TO_PREVIOUS], makeGetUserPlaytrack)
}

function* saga() {
    yield all([
        fork(startResumeUserPlaytrackSaga),
        fork(pauseUserlPlaytrackSaga),
        fork(getUserPlaytrackSaga),
        fork(playtrackNextSaga),
        fork(playtrackPreviousSaga),
        fork(updateOnSwitchPlaytrackSaga)
    ])
}

export default saga;