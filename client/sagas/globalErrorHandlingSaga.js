import {put, takeLatest} from "redux-saga/effects"

import {addError} from "actions/errorActions";

function* errorHandlingSaga(action) {
    yield put(addError(action.payload))
}

function* saga() {
    yield takeLatest(action => action.type.startsWith("FAIL"), errorHandlingSaga)
}

export default saga;