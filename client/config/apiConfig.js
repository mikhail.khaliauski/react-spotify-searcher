import axios from "axios";

import AuthenticationApi from "api/authenticationApi";

import {store} from "config/configureReduxApp";

import {logout} from "actions/authenticationActions";

import {getAccessToken, saveAccessToken} from "utils/sessionUtils";

//Interceptor of getting refresh token if access token expired;
export const responseInterceptor = axios.interceptors.response.use(
    response => {
        return response
    },
    error => {
        const {config, response: {status}} = error;
        const originalRequest = config;

        let refreshedSuccessfully = false;

        switch (status) {
            // If access token has expired then request for a new one and redo the call
            case 401:
                return AuthenticationApi.refreshAccessToken()
                    .then(newToken => {
                        saveAccessToken(newToken);
                        refreshedSuccessfully = true;
                        originalRequest.headers.Authorization = 'Bearer ' + newToken;
                        return Promise.resolve(axios(originalRequest));
                    })
                    .catch((err) => {
                        store.dispatch(logout());
                        throw err;
                    });
            default:
                return Promise.reject(error);
        }
    }
);

//Add Bearer token to Spotify API calls
export const requestInterceptor = axios.interceptors.request.use(config => {

    //detect spotify call
    if (!config.url.startsWith("https://api.spotify.com") && config.baseURL !== "https://api.spotify.com") {
        return config;
    }


    config.headers["Authorization"] = "Bearer " + getAccessToken();

    return config;
});

export default axios;