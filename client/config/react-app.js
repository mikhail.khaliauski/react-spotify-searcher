import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {Route} from 'react-router-dom';
import {ConnectedRouter} from "react-router-redux";
import {PersistGate} from "redux-persist/integration/react";

import {store, persistedStore, history} from "./configureReduxApp"

import App from "components/App"

import {URL_HOME} from "utils/sidebarRoutingUtils";

class ReactApp extends Component {
    static propTypes = {};
    static defaultProps = {};

    constructor() {
        super();
    }

    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistedStore}>
                    <ConnectedRouter history={history}>
                        <Route path={URL_HOME} component={App}/>
                    </ConnectedRouter>
                </PersistGate>
            </Provider>
        );
    }
}

export default ReactApp;