import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import {routerMiddleware} from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import createSagaReduxMiddleware from "redux-saga";
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {persistStore, persistReducer} from "redux-persist";
import storage from 'redux-persist/lib/storage/session';
import createEncryptor from "redux-persist-transform-encrypt";

import rootReducer from 'reducers';

import rootSaga from "sagas";


export const history = createHistory();

const router = routerMiddleware(createHistory());
const sagaMiddleware = createSagaReduxMiddleware();

const encryptModule = createEncryptor({
   secretKey: 'react-app'
});
const persistConfig = {
    key: "state",
    storage,
    transforms: [encryptModule]
};

const persistedRootReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
    persistedRootReducer,
    {},
    composeWithDevTools(
        applyMiddleware(thunk, logger, router, sagaMiddleware)
    )
);

export const persistedStore  = persistStore(store);

sagaMiddleware.run(rootSaga);