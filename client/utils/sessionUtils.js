const ACCESS_TOKEN_KEY = "ACCESS_TOKEN";


export const saveAccessToken = (token) => {
    sessionStorage.setItem(ACCESS_TOKEN_KEY, token);
};

export const getAccessToken = () => sessionStorage.getItem(ACCESS_TOKEN_KEY);

export const clearSession = () => sessionStorage.clear();