Number.prototype.betweenOrBorder = function (borderLeft, borderRight) {
    return (this >= +borderLeft && this <= +borderRight) ? this : (this <= +borderLeft ? +borderLeft : +borderRight);
};

