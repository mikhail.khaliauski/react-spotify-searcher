import React from 'react';

export const URL_HOME = "/";
export const URL_SEARCH = "/search";
export const URL_PLAY = "/playing";
export const URL_PLAYLISTS = "/playlists";
export const URL_LOGIN = "/login";

const navigationItems = Object.freeze({
    HOME: {
        name: "Spotify\nSearcher",
        url: URL_HOME,
        icon: "spotify"
    },
    SEARCH: {
        name: "Search",
        url: URL_SEARCH,
        icon: "search"
    },
    PLAYING: {
        name: "Playing",
        url: URL_PLAY,
        icon: "play"
    },
    PLAYLISTS: {
        name: "My Playlists",
        url: URL_PLAYLISTS,
        icon: "list"
    },
    LOGIN: {
        name: "Login",
        url: URL_LOGIN,
        icon: "sign-in"
    }
});

export const getNameByUrl = (url) => {
    return (Object.values(navigationItems).find(item => item.url.startsWith(url)) || {}).name || "";
};

export const logo = navigationItems.HOME;

export const authorizedTabs = [navigationItems.SEARCH, navigationItems.PLAYING, navigationItems.PLAYLISTS];
export const unauthorizedTabs = [navigationItems.LOGIN];