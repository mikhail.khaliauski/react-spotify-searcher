import PropTypes from "prop-types";


export const trackShape = PropTypes.shape({
    id: PropTypes.string,
    imageUrl: PropTypes.string,
    name: PropTypes.string,
    description: PropTypes.string
});