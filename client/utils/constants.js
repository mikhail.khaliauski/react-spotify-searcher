export const SEARCH_RESULTS_PER_PAGE = 20;


export const DISPLAY_MODE_GRID = "GRID";
export const DISPLAY_MODE_LIST = "LIST";

export const DISPLAY_MODE_GRID_ICON = "th-large";
export const DISPLAY_MODE_LIST_ICON = "list";




export const UNKNOWN_ERROR_MESSAGE = "Unknown error. Please, reach out to system administrator.";
export const USER_NOT_PREMIUM_ERROR_MESSAGE = "User is not premium.";
export const MAX_ATTEMPTS_EXCEEDED_ERROR_MESSAGE = "System is currently unavailable. Please, try later.";
export const DEVICE_NOT_FOUND_ERROR_MESSAGE = "Device not found.";