/*{
      "album" : {
        "album_type" : "album",
        "artists" : [ {
          "external_urls" : {
            "spotify" : "https://open.spotify.com/artist/5JdGGCCPo07UPcbg27cQXT"
          },
          "href" : "https://api.spotify.com/v1/artists/5JdGGCCPo07UPcbg27cQXT",
          "id" : "5JdGGCCPo07UPcbg27cQXT",
          "name" : "Лолита",
          "type" : "artist",
          "uri" : "spotify:artist:5JdGGCCPo07UPcbg27cQXT"
        } ],
        "available_markets" : [ "AD", "AR", "AT", "AU", "BE", "BG", "BO", "BR", "CA", "CH", "CL", "CO", "CR", "CY", "CZ", "DE", "DK", "DO", "EC", "EE", "ES", "FI", "FR", "GB", "GR", "GT", "HK", "HN", "HU", "ID", "IE", "IS", "IT", "JP", "LI", "LT", "LU", "LV", "MC", "MT", "MX", "MY", "NI", "NL", "NO", "NZ", "PA", "PE", "PH", "PL", "PT", "PY", "SE", "SG", "SK", "SV", "TH", "TR", "TW", "US", "UY" ],
        "external_urls" : {
          "spotify" : "https://open.spotify.com/album/1nZzCoAbN6iKA9rMMcDxKJ"
        },
        "href" : "https://api.spotify.com/v1/albums/1nZzCoAbN6iKA9rMMcDxKJ",
        "id" : "1nZzCoAbN6iKA9rMMcDxKJ",
        "images" : [ {
          "height" : 640,
          "url" : "https://i.scdn.co/image/439d7e0ac90bb379c6f53bbc2947661e144eba46",
          "width" : 640
        }, {
          "height" : 300,
          "url" : "https://i.scdn.co/image/46e96fe759716cdd710e5cc9e39f0b1002591c42",
          "width" : 300
        }, {
          "height" : 64,
          "url" : "https://i.scdn.co/image/deb4909298e640120c445a97d4eaa4d366c0e242",
          "width" : 64
        } ],
        "name" : "Ориентация север",
        "release_date" : "2012-10-11",
        "release_date_precision" : "day",
        "type" : "album",
        "uri" : "spotify:album:1nZzCoAbN6iKA9rMMcDxKJ"
      },
      "artists" : [ {
        "external_urls" : {
          "spotify" : "https://open.spotify.com/artist/5JdGGCCPo07UPcbg27cQXT"
        },
        "href" : "https://api.spotify.com/v1/artists/5JdGGCCPo07UPcbg27cQXT",
        "id" : "5JdGGCCPo07UPcbg27cQXT",
        "name" : "Лолита",
        "type" : "artist",
        "uri" : "spotify:artist:5JdGGCCPo07UPcbg27cQXT"
      } ],
      "available_markets" : [ "AD", "AR", "AT", "AU", "BE", "BG", "BO", "BR", "CA", "CH", "CL", "CO", "CR", "CY", "CZ", "DE", "DK", "DO", "EC", "EE", "ES", "FI", "FR", "GB", "GR", "GT", "HK", "HN", "HU", "ID", "IE", "IS", "IT", "JP", "LI", "LT", "LU", "LV", "MC", "MT", "MX", "MY", "NI", "NL", "NO", "NZ", "PA", "PE", "PH", "PL", "PT", "PY", "SE", "SG", "SK", "SV", "TH", "TR", "TW", "US", "UY" ],
      "disc_number" : 1,
      "duration_ms" : 198400,
      "explicit" : false,
      "external_ids" : {
        "isrc" : "UAMU11715032"
      },
      "external_urls" : {
        "spotify" : "https://open.spotify.com/track/76bYnDNh3mFyVfAMVh4qQZ"
      },
      "href" : "https://api.spotify.com/v1/tracks/76bYnDNh3mFyVfAMVh4qQZ",
      "id" : "76bYnDNh3mFyVfAMVh4qQZ",
      "name" : "Ориентация север",
      "popularity" : 31,
      "preview_url" : "https://p.scdn.co/mp3-preview/9fcaced52feb3b8be4108ce83f9272dad60692e0?cid=8897482848704f2a8f8d7c79726a70d4",
      "track_number" : 1,
      "type" : "track",
      "uri" : "spotify:track:76bYnDNh3mFyVfAMVh4qQZ"
    }*/


class PlayerApiMock {

    __playTrackResult = {};
    __pauseTrackResult = {};
    __getPlayingResult = {
        track: {
            id: "1nZzCoAbN6iKA9rMMcDxKJ",
            name: "Ориентация север",
            artists: "Лолита",
            album: "Ориентация север",
            previewUrl: "https://p.scdn.co/mp3-preview/9fcaced52feb3b8be4108ce83f9272dad60692e0?cid=8897482848704f2a8f8d7c79726a70d4",
            imageUrl: "https://i.scdn.co/image/439d7e0ac90bb379c6f53bbc2947661e144eba46"
        },
        isPlaying: false
    };

    playTrack = (trackId, currentAttempt = 0) => Promise.resolve(this.__playTrackResult);
    pauseTrack = (currentAttempt = 0) => Promise.resolve(this.__pauseTrackResult);
    getPlaying = () => Promise.resolve(this.__getPlayingResult);
    next = () => Promise.resolve();
    previous = () => Promise.resolve();
}

export default new PlayerApiMock();