import {clearSession, saveAccessToken} from "config/apiConfig";

class AuthenticationApi {

    __getUserResultPremium = {
        "provider": "spotify",
        "id": "21qhg6zyoao2gxipkepursnui",
        "username": "21qhg6zyoao2gxipkepursnui",
        "displayName": "Mike  Khaliavsky",
        "profileUrl": "https://open.spotify.com/user/21qhg6zyoao2gxipkepursnui",
        "photos": [
            "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/22730531_1707353042671881_3102989268873579632_n.jpg?oh=3fe4ad8686bad2c6c45b7eaf4a1c0484&oe=5B016CEA"
        ],
        "country": "US",
        "followers": 0,
        "product": "open",
        "_raw": "{\n  \"country\" : \"US\",\n  \"display_name\" : \"Mike  Khaliavsky\",\n  \"external_urls\" : {\n    \"spotify\" : \"https://open.spotify.com/user/21qhg6zyoao2gxipkepursnui\"\n  },\n  \"followers\" : {\n    \"href\" : null,\n    \"total\" : 0\n  },\n  \"href\" : \"https://api.spotify.com/v1/users/21qhg6zyoao2gxipkepursnui\",\n  \"id\" : \"21qhg6zyoao2gxipkepursnui\",\n  \"images\" : [ {\n    \"height\" : null,\n    \"url\" : \"https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/22730531_1707353042671881_3102989268873579632_n.jpg?oh=3fe4ad8686bad2c6c45b7eaf4a1c0484&oe=5B016CEA\",\n    \"width\" : null\n  } ],\n  \"product\" : \"open\",\n  \"type\" : \"user\",\n  \"uri\" : \"spotify:user:21qhg6zyoao2gxipkepursnui\"\n}",
        "_json": {
            "country": "US",
            "display_name": "Mike  Khaliavsky",
            "external_urls": {
                "spotify": "https://open.spotify.com/user/21qhg6zyoao2gxipkepursnui"
            },
            "followers": {
                "href": null,
                "total": 0
            },
            "href": "https://api.spotify.com/v1/users/21qhg6zyoao2gxipkepursnui",
            "id": "21qhg6zyoao2gxipkepursnui",
            "images": [
                {
                    "height": null,
                    "url": "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/22730531_1707353042671881_3102989268873579632_n.jpg?oh=3fe4ad8686bad2c6c45b7eaf4a1c0484&oe=5B016CEA",
                    "width": null
                }
            ],
            "product": "premium",
            "type": "user",
            "uri": "spotify:user:21qhg6zyoao2gxipkepursnui"
        },
        "accessToken": "BQBRxq5KLKinFoItDm7PdxSklk7k_Xnb_iZNwYXudZN6PF6e5W3MGr6xBR4OWYFfEKq__BNSkuDExK6SBInaVRvT5VSRNFdE8y2jmyHR2gqdyS8hAHm7WiWD4y1eZULLUDFprUSFehptkPRHknRfwSNLZb75uhEFiiHaPANIEdlQvD_XtwUiyYpDE5fsZd_xR0dmmQ"
    };

    getUser = () => Promise.resolve(this.__getUserResultPremium);

    refreshAccessToken = () => {throw Error("Not implemented for test.")}
}

export default new AuthenticationApi;