import axios from 'axios';

import {SEARCH_RESULTS_PER_PAGE} from "utils/constants";

export const URL_API_SEARCH = "https://api.spotify.com/v1/search";

class SearchApi {

    search = (key, offset = 0) => {
        return axios.get(URL_API_SEARCH, {
            params: {
                q: key.replace(' ', '+'),
                offset: offset,
                type: "track"
            }
        })
            .then((res) => {

                return {
                    total: res.data.tracks.total,
                    page: Math.floor(res.data.tracks.offset / SEARCH_RESULTS_PER_PAGE) + 1,
                    items: res.data.tracks.items.map(item => ({
                        id: item.id,
                        name: item.name,
                        artists: item.artists.map(artist => artist.name).reduce((res, current, i) => res + (i > 0 && ", ") + current),
                        album: item.album.name,
                        previewUrl: item.preview_url,
                        imageUrl: item.album.images[0].url
                    }))
                };
            });
    };
}

export default new SearchApi;