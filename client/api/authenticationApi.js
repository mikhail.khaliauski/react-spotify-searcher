import axios from 'axios';
import {clearSession, saveAccessToken} from "config/apiConfig";

export const URL_API_LOGIN = "/api/authentication/login";
export const URL_API_REFRESH_TOKEN = "/api/authentication/login/token";
export const URL_API_LOGOUT = "/api/authentication/logout";
export const URL_API_GET_ME = "/api/authentication/me";

class AuthenticationApi {

    constructor() {
        this.axios = axios.create();
    }

    getUser = () => {
        return this.axios.post(URL_API_GET_ME)
            .then((res) => {
                if (res.error) {
                    throw new Error(res.error);
                }

                return res.data
            });
    };

    refreshAccessToken =() => {
        return this.axios.post(URL_API_REFRESH_TOKEN)
            .then((res) => {
                if (res.data.error) {
                    throw new Error(res.data.error)
                }

                return res.data;
            })

    }
}

export default new AuthenticationApi;