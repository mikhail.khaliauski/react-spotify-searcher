import axios from 'axios';
import {
    DEVICE_NOT_FOUND_ERROR_MESSAGE, MAX_ATTEMPTS_EXCEEDED_ERROR_MESSAGE, UNKNOWN_ERROR_MESSAGE,
    USER_NOT_PREMIUM_ERROR_MESSAGE
} from "utils/constants";

export const URL_API_PLAY_USER_PLAYTRACK = "https://api.spotify.com/v1/me/player/play";
export const URL_API_PAUSE_USER_PLAYTRACK = "https://api.spotify.com/v1/me/player/pause";
export const URL_API_GET_USER_PLAYTRACK = "https://api.spotify.com/v1/me/player/currently-playing";
export const URL_API_SWITCH_PLAYTRACK_NEXT = "https://api.spotify.com/v1/me/player/next";
export const URL_API_SWITCH_PLAYTRACK_PREVIOUS= "https://api.spotify.com/v1/me/player/previous";

class PlayerApi {

    playTrack = (trackId, currentAttempt = 0) => {
        const maxAttempts = 5;
        let data = {};

        if (trackId) {
            data.uris = [`spotify:track:${trackId}`];
        }

        return axios.put(URL_API_PLAY_USER_PLAYTRACK, {...data})
            .then(res => {
                if (res.status === 204) {
                    return Promise.resolve(res.data);
                }
                if (res.status === 202) {
                    currentAttempt++;
                    if (currentAttempt < maxAttempts) {
                        return setTimeout(() => {
                            return this.playTrack(trackId, currentAttempt);
                        }, 5000);
                    }
                    throw Error({
                        response: {
                            status: 405
                        }
                    })
                }
            })
            .catch(err => {
                let message;
                switch (err.response.status) {
                    case 403:
                        message = USER_NOT_PREMIUM_ERROR_MESSAGE;
                        break;
                    case 405:
                        message = MAX_ATTEMPTS_EXCEEDED_ERROR_MESSAGE;
                        break;
                    default:
                        message = UNKNOWN_ERROR_MESSAGE;
                        break;
                }

                return Promise.reject(message)
            });
    };

    pauseTrack = (currentAttempt = 0) => {

        const maxAttempts = 5;

        return axios.put(URL_API_PAUSE_USER_PLAYTRACK)
            .then(res => {
                if (res.status === 204) {
                    return Promise.resolve(res.data);
                }
                if (res.status === 202) {
                    currentAttempt++;
                    if (currentAttempt < maxAttempts) {
                        return setTimeout(() => {
                            return this.playTrack(trackId, currentAttempt);
                        }, 5000);
                    }
                    throw Error({
                        response: {
                            status: 405
                        }
                    })
                }
            })
            .catch(err => {
                let message;
                switch (err.response.status) {
                    case 403:
                        message = USER_NOT_PREMIUM_ERROR_MESSAGE;
                        break;
                    case 405:
                        message = MAX_ATTEMPTS_EXCEEDED_ERROR_MESSAGE;
                        break;
                    default:
                        message = UNKNOWN_ERROR_MESSAGE;
                        break;
                }

                return Promise.reject(message)
            });
    };


    getPlaying = () => {
        return axios.get(URL_API_GET_USER_PLAYTRACK)
            .then(res => {
                let track = null;
                let isPlaying = false;

                if (res.status === 200 && res.data) {

                    let item = res.data.item;
                    track = {
                        id: item.id,
                        name: item.name,
                        artists: item.artists.map(artist => artist.name).reduce((res, current, i) => res + (i > 0 && ", ") + current),
                        album: item.album.name,
                        previewUrl: item.preview_url,
                        imageUrl: item.album.images[0].url
                    };

                    isPlaying = res.data.is_playing
                }

                return {
                    track,
                    isPlaying
                }
            });
    };

    next = () => {
        return axios.get(URL_API_SWITCH_PLAYTRACK_NEXT)
            .catch(err => {
                let message;
                switch (err.response.status) {
                    case 404:
                        message = DEVICE_NOT_FOUND_ERROR_MESSAGE;
                        break;
                    case 403:
                        message = USER_NOT_PREMIUM_ERROR_MESSAGE;
                        break;
                    default:
                        message = UNKNOWN_ERROR_MESSAGE;
                        break;
                }

                return Promise.reject(message);
            });
    };

    previous = () => {
        return axios.get(URL_API_SWITCH_PLAYTRACK_PREVIOUS)
            .catch(err => {
                let message;
                switch (err.response.status) {
                    case 404:
                        message = DEVICE_NOT_FOUND_ERROR_MESSAGE;
                        break;
                    case 403:
                        message = USER_NOT_PREMIUM_ERROR_MESSAGE;
                        break;
                    default:
                        message = UNKNOWN_ERROR_MESSAGE;
                        break;
                }

                return Promise.reject(message);
            });
    }
}

export default new PlayerApi;