import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap';
import "animate.css";
import ReactApp from 'config/react-app'
import 'config/apiConfig';
import 'utils/extensions';

import 'font-awesome/scss/font-awesome.scss';
import 'css'


ReactDOM.render(<ReactApp/>, document.getElementById('root'));
