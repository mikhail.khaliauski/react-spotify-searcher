export const LOGOUT = "LOGOUT";

export const BEGIN_FETCH_USER = "BEGIN_FETCH_USER";
export const SUCCESS_FETCH_USER = "SUCCESS_FETCH_USER";
export const FAIL_FETCH_USER = "FAIL_FETCH_USER";

export const beginFetchUser = () => ({
    type: BEGIN_FETCH_USER,
});
export const successFetchUser = (user) => ({
    type: SUCCESS_FETCH_USER,
    payload: user
});
export const failFetchUser = () => ({
    type: FAIL_FETCH_USER,
});

export const logout = () => ({
    type: LOGOUT
});


