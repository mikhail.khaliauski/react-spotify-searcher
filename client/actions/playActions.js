export const BEGIN_START_USER_PLAYTRACK = "BEGIN_START_USER_PLAYTRACK";
export const SUCCESS_START_USER_PLAYTRACK = "SUCCESS_START_USER_PLAYTRACK";
export const FAIL_START_USER_PLAYTRACK = "FAIL_START_USER_PLAYTRACK";

export const beginStartUserPlayTrack = (track) => ({
    type: BEGIN_START_USER_PLAYTRACK,
    payload: track
});
export const successStartUserPlayTrack = (track) => ({
    type: SUCCESS_START_USER_PLAYTRACK,
    payload: track
});
export const failStartUserPlayTrack = (payload) => ({
    type: FAIL_START_USER_PLAYTRACK,
    payload: {
        id: Date.now(),
        message: payload
    }
});


export const BEGIN_PAUSE_USER_PLAYTRACK = "BEGIN_PAUSE_USER_PLAYTRACK";
export const SUCCESS_PAUSE_USER_PLAYTRACK = "SUCCESS_PAUSE_USER_PLAYTRACK";
export const FAIL_PAUSE_USER_PLAYTRACK = "FAIL_PAUSE_USER_PLAYTRACK";

export const beginPauseUserPlayTrack = () => ({
    type: BEGIN_PAUSE_USER_PLAYTRACK,
});
export const successPauseUserPlayTrack = () => ({
    type: SUCCESS_PAUSE_USER_PLAYTRACK,
});
export const failPauseUserPlayTrack = (payload) => ({
    type: FAIL_PAUSE_USER_PLAYTRACK,
    payload: {
        id: Date.now(),
        message: payload
    }
});

/**/

export const BEGIN_GET_USER_PLAYTRACK = "BEGIN_GET_USER_PLAYTRACK";
export const SUCCESS_GET_USER_PLAYTRACK = "SUCCESS_GET_USER_PLAYTRACK";
export const FAIL_GET_USER_PLAYTRACK = "FAIL_GET_USER_PLAYTRACK";

export const beginGetUserPlaytrack = () => ({
    type: BEGIN_GET_USER_PLAYTRACK,
});
export const successGetUserPlaytrack = (payload) => ({
    type: SUCCESS_GET_USER_PLAYTRACK,
    payload
});
export const failGetUserPlaytrack = (payload) => ({
    type: FAIL_GET_USER_PLAYTRACK,
    payload
});


export const PLAY_PREVIEW = "PLAY_PREVIEW";

export const playPreview = (track) => ({
    type: PLAY_PREVIEW,
    payload: track
});


export const PAUSE_PREVIEW = "PAUSE_PREVIEW";

export const pausePreview = () => ({
    type: PAUSE_PREVIEW,
});


export const CLEAR_PREVIEW = "CLEAR_PREVIEW";

export const clearPreview = () => ({
    type: CLEAR_PREVIEW,
});

export const BEGIN_SWITCH_USER_PLAYTRACK_TO_NEXT = "BEGIN_SWITCH_USER_PLAYTRACK_TO_NEXT";
export const SUCCESS_SWITCH_USER_PLAYTRACK_TO_NEXT = "SUCCESS_SWITCH_USER_PLAYTRACK_TO_NEXT";
export const FAIL_SWITCH_USER_PLAYTRACK_TO_NEXT = "FAIL_SWITCH_USER_PLAYTRACK_TO_NEXT";

export const beginSwitchUserPlaytrackToNext = () => ({
    type: BEGIN_SWITCH_USER_PLAYTRACK_TO_NEXT,
});
export const successPlaytrackNext = () => ({
    type: SUCCESS_SWITCH_USER_PLAYTRACK_TO_NEXT,
});
export const failPlaytrackNext = (payload) => ({
    type: FAIL_SWITCH_USER_PLAYTRACK_TO_NEXT,
    payload
});


export const BEGIN_SWITCH_USER_PLAYTRACK_TO_PREVIOUS = "BEGIN_SWITCH_USER_PLAYTRACK_TO_PREVIOUS";
export const SUCCESS_SWITCH_USER_PLAYTRACK_TO_PREVIOUS = "SUCCESS_SWITCH_USER_PLAYTRACK_TO_PREVIOUS";
export const FAIL_SWITCH_USER_PLAYTRACK_TO_PREVIOUS = "FAIL_SWITCH_USER_PLAYTRACK_TO_PREVIOUS";

export const beginSwitchUserPlaytrackToPrevious = () => ({
    type: BEGIN_SWITCH_USER_PLAYTRACK_TO_PREVIOUS,
});
export const successSwitchUserPlaytrackToPrevious = () => ({
    type: SUCCESS_SWITCH_USER_PLAYTRACK_TO_PREVIOUS,
});
export const failSwitchUserPlaytrackToPrevious = (payload) => ({
    type: FAIL_SWITCH_USER_PLAYTRACK_TO_PREVIOUS,
    payload
});

