export const BEGIN_SEARCH = "BEGIN_SEARCH";
export const SUCCESS_SEARCH = "SUCCESS_SEARCH";
export const FAIL_SEARCH = "FAIL_SEARCH";
export const CLEAR_SEARCH = "CLEAR_SEARCH";

export const beginSearch = () => ({
    type: BEGIN_SEARCH
});

export const successSearch = (payload) => ({
    type: SUCCESS_SEARCH,
    payload
});

export const clearSearch = () => ({
    type: CLEAR_SEARCH
});

export const failSearch = () => ({
    type: FAIL_SEARCH,
});

export const UPDATE_SEARCH_TEXT = "UPDATE_SEARCH_TEXT";

export const updateSearchText = (text) => ({
    type: UPDATE_SEARCH_TEXT,
    payload: text
});

export const SET_SEARCH_DISPLAY_MODE = "SET_SEARCH_DISPLAY_MODE";


export const setSearchDisplayMode = (mode) => ({
    type: SET_SEARCH_DISPLAY_MODE,
    payload: mode
});


export const SET_SEARCH_PAGE = "SET_SEARCH_PAGE";

export const setSearchPage = (page) => ({
    type: SET_SEARCH_PAGE,
    payload: page
});




