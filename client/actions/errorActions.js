export const ADD_ERROR = "ADD_ERROR";
export const REMOVE_ERROR = "REMOVE_ERROR";

export const addError = (err) => ({
    type: ADD_ERROR,
    payload: err
});

export const removeError = (id) => ({
    type: REMOVE_ERROR,
    payload: id
});