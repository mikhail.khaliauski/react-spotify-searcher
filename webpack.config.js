"use strict";
const path = require("path");

const webpack = require("webpack");

const ProvidePlugin = webpack.ProvidePlugin;
const CleanWebpackPlugin = require('clean-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');

module.exports = {
    entry: {
        app: path.resolve(__dirname, "client", "index.js"),
    },
    plugins: [
        new CleanWebpackPlugin([path.resolve(__dirname, "build")]),
        new ManifestPlugin(),
        new ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            Popper: ['popper.js', 'default']
        })
    ],
    output: {
        filename: "[name].js",
        path: path.resolve(__dirname, "build"),
        publicPath: "/"
    },
    module: {
        rules: [
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    "file-loader"
                ]
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: [
                    "babel-loader"
                ]
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader']
            },
            {
                test: require.resolve("jquery"),
                use: [
                    {
                        loader: "expose-loader",
                        options: "$"
                    },
                    {
                        loader: "expose-loader",
                        options: "jQuery"
                    },
                    {
                        loader: "expose-loader",
                        options: "window.$"
                    },
                    {
                        loader: "expose-loader",
                        options: "window.jQuery"
                    }
                ]
            },
            {
                test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                // Limiting the size of the woff fonts breaks font-awesome ONLY for the extract text plugin
                // loader: "url?limit=10000"
                use: "url-loader"
            },
            {
                test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
                use: 'file-loader'
            }
        ]
    },
    resolve: {
        extensions: [".js", ".jsx"],
        alias: {
            components: path.resolve(__dirname, "client", "components"),
            actions: path.resolve(__dirname, "client", "actions"),
            css: path.resolve(__dirname, "public", "styles", "index.scss"),
            utils: path.resolve(__dirname, "client", "utils"),
            reducers: path.resolve(__dirname, "client", "reducers"),
            api: path.resolve(__dirname, "client", "api"),
            config: path.resolve(__dirname, "client", "config"),
            sagas: path.resolve(__dirname, "client", "sagas")
        }
    }
};