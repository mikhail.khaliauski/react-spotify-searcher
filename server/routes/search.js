import express from 'express';

const router = express.Router();

import axios from "axios";


router.get("/search", (req, res) => {
    axios.get("/v1/search", {
        baseURL: "https://api.spotify.com",
        headers: {
            Authorization: "Bearer " + req.user.accessToken
        },
        params: {
            q: req.query.key.replace(" ", "+"),
            type: "track",
            limit: req.params.limit,
            offset: req.params.offset,
        }
    }).then((res) => {
        if (res.status === 200) {
            res.status(200).send(res.data.tracks)
        }

        res.send(data.statusText);
    })
});


export default router;