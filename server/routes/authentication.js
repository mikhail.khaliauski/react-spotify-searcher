import express from 'express';

const router = express.Router();

import passport from 'passport'
import {Strategy as SpotifyStrategy} from 'passport-spotify';
import refresh from 'passport-oauth2-refresh';
import {
    STATUS_CODE_OK,
    STATUS_CODE_SERVER_ERROR,
    STATUS_CODE_UNAUTHORIZED
} from "../utils/statusCodes";


let clientID = "28562c09ec78421c9a1e2de0fa1cc932";
let clientSecret = "2fb1cad28482450f8dab9d6a747a0ce4";

console.log(process.env.PORT);
const strategy = new SpotifyStrategy({
        clientID,
        clientSecret,
        callbackURL: `http://localhost:${process.env.PORT}/api/authentication/login/callback`
    },
    (accessToken, refreshToken, profile, done) => {
        return done(null, {
            ...profile,
            accessToken,
            refreshToken,
        })
    }
);


passport.use('spotify', strategy);
refresh.use(strategy);


router.get('/login',
    passport.authenticate('spotify', {
        scope: [
            "user-library-read",
            "user-read-private",
            "user-read-currently-playing",
            "user-read-recently-played",
            "user-modify-playback-state",
            "playlist-read-private"
        ]
    }),
    (req, res) => {

    }
);

router.get('/login/callback',
    passport.authenticate('spotify')
);

router.post('/login/token',
    (req, res, next) => {
        if (!req.isAuthenticated()) {
            res.status(STATUS_CODE_UNAUTHORIZED).end();
        } else {
            next();
        }
    },
    (req, res) => {
        refresh.requestNewAccessToken("spotify", req.user.refreshToken, {}, (err, accessToken, refreshToken, ...other) => {
            if (err) {
                res.status(STATUS_CODE_SERVER_ERROR).send(err)
            }

            let resultAccessToken;
            req.logIn({
                ...req.user,
                accessToken,
            }, (err) => {
                if (err) {
                    return res.redirect("/");
                }
                resultAccessToken = accessToken;

            });

            res.status(STATUS_CODE_OK).send(resultAccessToken);
        })
    }
);

router.get('/logout', (req, res) => {
    req.logout();
    res.redirect("/");
});


router.post("/me", (req, res) => {
    if (req.isAuthenticated()) {
        let {refreshToken, ...user} = req.user;

        res.status(STATUS_CODE_OK).send(user);
    } else {
        res.status(STATUS_CODE_UNAUTHORIZED).end();
    }
});

export default router;