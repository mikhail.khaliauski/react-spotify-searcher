import express from 'express';
import morgan from 'morgan';
import path from 'path';
import passport from 'passport';
import bodyParser from 'body-parser';
import expressSession from 'express-session';

import authenticationRoutes from "./routes/authentication";
import searchRoutes from "./routes/search";

const app = express();

// Setup logger
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));

// Serve static assets
app.use(express.static(path.resolve(__dirname, '..', 'build')));

app.use(expressSession({
    name: "session",
    secret: "p1e@seD0ntSh@re1tW1thAnyb0dy",
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: false
    }
}));

// parse application/json
app.use(bodyParser.json());

app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, done) => {
    done(null, JSON.stringify(user));
});

passport.deserializeUser((user, done) => {
    done(null, JSON.parse(user));
});

let indexFilePath = process.env.NODE_ENV === "dev" ? path.resolve(__dirname, "..", "index.html") : path.resolve(__dirname, "..", "build", "index.html");


app.use("/api/authentication", authenticationRoutes);
app.use("/api/search", searchRoutes);


// Always return the main index.html, so react-router render the route in the client
app.get('*', (req, res) => {
    res.sendFile(indexFilePath)
});

module.exports = app;