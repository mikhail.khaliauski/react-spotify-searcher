'use strict';

if (!process.env.PORT) {
    process.env.PORT = process.env.NODE_ENV === "dev" ? 3000 : 9000;
}

const PORT = process.env.PORT;

app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}!`);
});

import app from './app';