"use strict";
const path = require("path");

const webpack = require("webpack");
const merge = require("webpack-merge");

const base = require("./webpack.config");

const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');


module.exports = merge(base, {
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new HtmlWebpackPlugin({
            filename: path.resolve(__dirname, "index.html"),
            template: path.resolve(__dirname, "public", "index.html"),
            alwaysWriteToDisk: true
        }),
        new HtmlWebpackHarddiskPlugin()
    ],
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'sass-loader',
                ],
            }
        ]
    },
    devtool: "source-map",
    devServer: {
        hot: true,
        proxy: {
            '**': {
                target: "http://localhost:9000"
            }
        }
    },
});